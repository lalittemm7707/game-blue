/** @format */

import React, { useContext, useEffect, useState } from "react";
import * as moment from "moment";
import Header from "./Header";
import { GameContext, LangContext } from "./gameContext";
import Campaigns from "./Campaigns";

const Main = () => {
	const [gameData] = useContext(GameContext); //fetching Game data
	const languageData = useContext(LangContext); //get data in selected language
	const [tab, selectTab] = useState(0); // For tab navigation
	const [live, setLive] = useState([]);
	const [past, setPast] = useState([]);
	const [upcoming, setUpcoming] = useState([]);

	// differentiating game data according to the date
	useEffect(() => {
		const tempU = [],
			tempL = [],
			tempP = [];
		gameData.map((game) => {
			const today = moment(new Date());
			if (today.diff(moment(new Date(game.date)), "days") < 0) tempU.push(game);
			else if (today.diff(moment(new Date(game.date)), "days") === 0)
				tempL.push(game);
			else {
				tempP.push(game);
			}
			return null;
		});
		setUpcoming(tempU);
		setLive(tempL);
		setPast(tempP);
	}, [gameData]);

	return (
		<div>
			<div className='tabs'>
				<ul>
					<li className={`${tab === 0 && "is-active"}`}>
						<a href='#' onClick={() => selectTab(0)}>
							{languageData.tabs.first}
						</a>
					</li>
					<li className={`${tab === 1 && "is-active"}`}>
						<a href='#' onClick={() => selectTab(1)}>
							{languageData.tabs.second}
						</a>
					</li>
					<li className={`${tab === 2 && "is-active"}`}>
						<a href='#' onClick={() => selectTab(2)}>
							{languageData.tabs.third}
						</a>
					</li>
				</ul>
			</div>
			<div className='box '>
				<Header />
				{tabs(tab, { past, live, upcoming })}
			</div>
		</div>
	);
};

// Tab Navigation Implementation
const tabs = (index, { past, live, upcoming }) => {
	switch (index) {
		case 0:
			return <Campaigns data={upcoming} />;
		case 1:
			return <Campaigns data={live} />;
		case 2:
			return <Campaigns data={past} />;
	}
};

export default Main;
