/** @format */

import React, { useContext } from "react";
import { LangContext } from "./gameContext";

const Header = () => {
	const languageData = useContext(LangContext); //get data in selected language
	return (
		<div className='columns has-background-light is-hidden-mobile'>
			<div className='column is-2'>{languageData.tableHead.first}</div>
			<div className='column is-3'>{languageData.tableHead.second}</div>
			<div className='column is-2'>{languageData.tableHead.third}</div>
			<div className='column '>{languageData.tableHead.fourth}</div>
		</div>
	);
};

export default Header;
