/** @format */

import React, { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDollarSign } from "@fortawesome/free-solid-svg-icons";
const PriceListing = () => {
	return (
		<Fragment>
			{priceData.map((item, index) => {
				return (
					<div className='columns is-flex-mobile' key={index}>
						<div className='column is-6 '>
							<span className='is-size-7 has-text-weight-semibold is-pulled-left'>
								{item.range}
							</span>
						</div>
						<div className='column has-text-right'>
							<span className='is-size-7 has-text-link'>
								<span className='has-text-weight-bold'>
									<FontAwesomeIcon icon={faDollarSign} />
									{item.price}
								</span>
							</span>
						</div>
					</div>
				);
			})}
		</Fragment>
	);
};

const priceData = [
	{ range: "1 Week - 1 Month", price: 100 },
	{ range: "6 Months", price: 500 },
	{ range: "1 Year", price: 900 },
];

export default PriceListing;
