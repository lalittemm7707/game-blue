/** @format */

import { createContext } from "react";

export const GameContext = createContext();
export const LangContext = createContext();
