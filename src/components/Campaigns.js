/** @format */

import React, { Fragment } from "react";
import GameInfo from "./GameInfo";

const Campaigns = ({ data }) => {
	return (
		<Fragment>
			{data.map((game) => (
				<Fragment key={game.id}>
					<GameInfo game={game} />
					<hr />
				</Fragment>
			))}
		</Fragment>
	);
};

export default Campaigns;
