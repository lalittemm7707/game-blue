/** @format */

import React, { useState, Fragment, useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faDollarSign,
	faFileCsv,
	faChartLine,
	faCalendarAlt,
} from "@fortawesome/free-solid-svg-icons";
import * as moment from "moment";
import DatePicker from "./DatePicker";
import { GameContext, LangContext } from "./gameContext";
import ViewPricing from "./ViewPricing";

const GameInfo = ({ game }) => {
	const [gameData, setGameData] = useContext(GameContext); // fetching truth
	const languageData = useContext(LangContext); //get data in selected language
	const [openPicker, setOpenPicker] = useState(false); // for Scheduler
	const [showPricing, setShowPricing] = useState(false);

	// Updating new date for game
	const setNewDate = (date) => {
		setOpenPicker(false);
		const temp = [...gameData];
		temp.map((e) => {
			if (e.id === game.id) {
				e.date = moment(date).format("YYYY/MM/DD");
			}
			return null;
		});
		setGameData(temp);
	};

	return (
		<Fragment>
			<div className='columns is-vcentered'>
				<div className='column is-2'>
					{moment(game.date).format("LL")}
					{moment(new Date()).diff(moment(new Date(game.date)), "days") !==
						0 && <p> {moment(Date.parse(game.date)).fromNow()}</p>}
				</div>
				<div className='column is-3'>
					<div className='is-flex' style={{ alignItems: "center" }}>
						<figure className='image is-64x64'>
							<img src={game.imageUrl} alt={game.name} />
						</figure>
						<div>
							<p>{game.name + " "}</p>
							<p className='is-size-6 has-text-grey'>({game.region})</p>
						</div>
					</div>
				</div>
				<div className='column is-2'>
					<FontAwesomeIcon
						icon={faDollarSign}
						color='yellow'
						style={{ marginRight: "5px" }}
					/>
					<span
						className='is-size-7'
						onClick={(_) => setShowPricing(!showPricing)}>
						{languageData.tableActions.first}
					</span>
				</div>
				<div className='column is-flex'>
					<FontAwesomeIcon
						icon={faFileCsv}
						color='green'
						style={{ marginRight: "5px" }}
					/>{" "}
					<span className='is-size-7' style={{ marginRight: "20px" }}>
						{languageData.tableActions.second}
					</span>
					<FontAwesomeIcon
						icon={faChartLine}
						color='orange'
						style={{ marginRight: "5px" }}
					/>
					<span className='is-size-7' style={{ marginRight: "20px" }}>
						{languageData.tableActions.third}
					</span>
					{openPicker && (
						<div className='column-1'>
							<button
								className='is-danger button is-small'
								onClick={() => setOpenPicker(false)}>
								X
							</button>
						</div>
					)}
					{!openPicker ? (
						<Fragment>
							<FontAwesomeIcon
								icon={faCalendarAlt}
								color='blue'
								style={{ marginRight: "5px" }}
							/>
							<span
								className='is-size-7'
								style={{ marginRight: "20px" }}
								onClick={() => setOpenPicker(!openPicker)}>
								{languageData.tableActions.fourth}
							</span>
						</Fragment>
					) : (
						<Fragment>
							<DatePicker
								selected={new Date(game.date)}
								onChange={(date) => setNewDate(date)}
								onBlur={() => console.log("sd")}
								onFocusOut={() => console.log("sd")}
							/>
						</Fragment>
					)}
				</div>
			</div>
			<ViewPricing
				game={game}
				active={showPricing}
				closePricing={setShowPricing}
			/>
		</Fragment>
	);
};
export default GameInfo;
