/** @format */

import React from "react";
import "react-datepicker/dist/react-datepicker.css";
import ReactDatePicker from "react-datepicker";
const DatePicker = ({ ...props }) => {
	return <ReactDatePicker {...props} inline />;
};

export default DatePicker;
