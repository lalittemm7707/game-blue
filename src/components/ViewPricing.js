/** @format */

import React from "react";
import PriceListing from "./PriceListing";

const ViewPricing = ({ game, active = true, closePricing }) => {
	return (
		<div className='App'>
			<div className={`modal ${active && "is-active"}`}>
				<div className='card box'>
					<div className='card-content'>
						<div className='media'>
							<div className='media-left'>
								<figure className='image is-128x128'>
									<img src={game.imageUrl} alt={game.name} />
								</figure>
							</div>
							<div className='media-content'>
								<p className='subtitle is-6 has-text-weight-semibold is-capitalized'>
									{game.name}
								</p>
								<p className='subtitle is-6 is-pulled-left'>{game.region}</p>
							</div>
						</div>

						<div className='content'>
							<PriceListing />
						</div>
					</div>
					<button
						className='button is-outlined'
						onClick={() => closePricing(false)}>
						Close
					</button>
				</div>
			</div>
		</div>
	);
};

export default ViewPricing;
