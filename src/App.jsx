/** @format */

import React, { useState, useEffect } from "react";
import "./style.scss";
import "./App.css";
import Main from "./components/Main";
import { GameContext, LangContext } from "./components/gameContext";
import { getSelectedLanguage } from "./assets/language";

function App() {
	const [gameData, setGameData] = useState(data); // Single source of truth for gameData
	const [lang, setLang] = useState("en");
	const [languageData, setLanguageData] = useState(getSelectedLanguage("en"));
	useEffect(() => {
		setLanguageData(getSelectedLanguage(lang));
	}, [lang]);
	return (
		<LangContext.Provider value={languageData}>
			<GameContext.Provider value={[gameData, setGameData]}>
				<section className='section container'>
					<p className='title '>{languageData.title}</p>
					<div className='select is-pulled-right'>
						<select value={lang} onChange={(e) => setLang(e.target.value)}>
							<option value='en'>English</option>
							<option value='gn'>German</option>
						</select>
					</div>
					<Main />
				</section>
			</GameContext.Provider>
		</LangContext.Provider>
	);
}

// sample Data for App added one more unique identifier called id
const data = [
	{
		name: "Summoners War",
		region: "US",
		createdOn: 1559807714999,
		price: "Price info of Test Whatsapp",
		csv: "Some CSV link for Whatsapp",
		report: "Some report link for Whatsapp",
		imageUrl:
			"https://lh3.googleusercontent.com/UbdpbnAWSOFU_35tULIOPLmV5ey0bq6NTL59Ko7nNfig8WqNPbO3xAHsoQA9Sk8-_V0=s180-rw",
		date: "2019/11/11",
		id: "001",
	},
	{
		name: "Super Jewels Quest",
		region: "CA, FR",
		createdOn: 1559806715124,
		price: "Price info of Super Jewels Quest",
		csv: "Some CSV link for Super Jewels Quest",
		report: "Some report link for Super Jewels Ques",
		imageUrl:
			"https://lh3.googleusercontent.com/3pQQ6FM2i3QnUQXyAFHtSvugfWRmBGCERQpohtSdeJBQDv3Dd429fkZmzvcSC6X3P3XT=s180-rw",
		date: "2019/11/11",
		id: "002",
	},
	{
		name: "Mole Slayer",
		region: "FR",
		createdOn: 1559806711124,
		price: "Price info of Mole Slayer",
		csv: "Some CSV link for Mole Slayer",
		report: "Some report link for Mole Slayer",
		imageUrl:
			"https://lh3.googleusercontent.com/80nkj4tsWLpZdwIGU2lqw1ccYbPwcvvsaxUOietMJEVHSG2cMDAoldkC_Hndv9MNZrM=s180-rw",
		date: "2020/11/11",
		id: "003",
	},
	{
		name: "Mancala Mix",
		region: "JP",
		createdOn: 1559806680124,
		price: "Price info of Mancala Mix",
		csv: "Some CSV link for Mancala Mix",
		report: "Some report link for Mancala Mix",
		imageUrl:
			"https://lh3.googleusercontent.com/KGIwunOpKNZ8Vbq9DtePSHzW21EI0HC_w3_3wWy3-926_1QnEq0aaUxnZh40tmNuMJk=s180-rw",
		date: "2020/08/01",
		id: "004",
	},
	{
		name: "Garena free fire",
		region: "JP",
		createdOn: 1559806680124,
		price: "Price info of Mancala Mix",
		csv: "Some CSV link for Mancala Mix",
		report: "Some report link for Mancala Mix",
		imageUrl:
			"https://lh3.googleusercontent.com/lKb7KUgesFXQLIkSZrACkP6tZGmql0la5EqYXbOxpcR_LJPbly3OEhzmjX7Hyq_0Nx4=s180-rw",
		date: "2019/10/28",
		id: "005",
	},
	{
		name: "MORTAL KOMBAT",
		region: "US",
		createdOn: 1559806680124,
		price: "Price info of Mancala Mix",
		csv: "Some CSV link for Mancala Mix",
		report: "Some report link for Mancala Mix",
		imageUrl:
			"https://lh3.googleusercontent.com/xguhLsWrYu21zWJZ4y-sEbsFVPUFNMe5AWJMf_Udu2cOnPjWqLf1gGsEbrugQS6HUnk=s180-rw",
		date: "2020/08/03",
		id: "006",
	},
	{
		name: "Auto Chess",
		region: "US",
		createdOn: 1559806680124,
		price: "Price info of Mancala Mix",
		csv: "Some CSV link for Mancala Mix",
		report: "Some report link for Mancala Mix",
		imageUrl:
			"https://lh3.googleusercontent.com/h5mpoRJlRE67-ZGTn_-eU6kiHTFJsCCeXCPxOvlZ_ZRZv_b8f1kuEL9KmFju8SC93eA=s180-rw",
		date: "2020/08/08",
		id: "007",
	},
	{
		name: "Need for Speed™ No Limits",
		region: "US",
		createdOn: 1559806680124,
		price: "Price info of Mancala Mix",
		csv: "Some CSV link for Mancala Mix",
		report: "Some report link for Mancala Mix",
		imageUrl:
			"https://lh3.googleusercontent.com/2sK95F75KkGRs6SIjD5_7W4r9rK-XmptFa3oAgZmCenqiq0FsdpHwvrov77gscNrgA=s180-rw",
		date: "2020/08/10",
		id: "008",
	},
	{
		name: "Shadow Fight 3",
		region: "US",
		createdOn: 1559806680124,
		price: "Price info of Mancala Mix",
		csv: "Some CSV link for Mancala Mix",
		report: "Some report link for Mancala Mix",
		imageUrl:
			"https://lh3.googleusercontent.com/IO0mRQrnEhP3OLbemjLOJArxs41FtXxhIHSekqqxfpDvbA-QO_yRRPrkxg85vpb3jU09=s180-rw",
		date: "2020/08/17",
		id: "009",
	},
];

export default App;
