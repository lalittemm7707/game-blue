/** @format */

const languages = {
	en: {
		title: "Manage Campaigns",
		tabs: {
			first: "Upcoming Campaigns",
			second: "Live Campaigns",
			third: "Past campaigns",
		},
		tableHead: {
			first: "Date",
			second: "Campaigns",
			third: "View",
			fourth: "Actions",
		},
		tableActions: {
			first: "View Pricing",
			second: "CSV",
			third: "Report",
			fourth: "Schedule Again",
		},
	},
	gn: {
		title: "Kampagnen verwalten",
		tabs: {
			first: "Kommende Kampagnen",
			second: "Live-Kampagnen",
			third: "Vergangene Kampagnen",
		},
		tableHead: {
			first: "Datum",
			second: "Kampagnen",
			third: "Aussicht",
			fourth: "Aktionen",
		},
		tableActions: {
			first: "Preise anzeigen",
			second: "CSV",
			third: "Bericht",
			fourth: "Planen Sie erneut",
		},
	},
};

export const getSelectedLanguage = (lang) => {
	return languages[lang];
};
